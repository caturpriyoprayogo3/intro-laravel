<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<h1>
  Buat Account Baru!
</h1>
<h2>
  Sign Up Form
</h2>
<form action="/welcome" method="post">
    @csrf <!-- ini adalah token -->
  <label for="">First name:</label><br>
  <input type="text" name="fname"><br><br>
  <label for="">Last name:</label><br>
  <input type="text" name="lname"><br><br>
  <label for="">Gender</label><br>
  <input type="radio"  name="Gender">Male<br>
  <input type="radio" name="Gender">Female<br>
  <input type="radio" name="Gender">Other<br><br>
  <label for="">Nationality:</label><br>
  <select name="nationality">
    <option value="Indonesia">Indonesia</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Thailand">Thailand</option>
  </select><br><br>
  <label for="">Language spoken:</label><br>
  <input type="checkbox">Bahasa Indonesia<br>
  <input type="checkbox">English<br>
  <input type="checkbox">Other<br><br>
  <label for="">Bio:</label><br>
  <textarea name="biodata" id="" cols="30" rows="10"></textarea><br>
  <input type="submit" value="Sign Up">



</form>
</body>
</html>